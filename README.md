# [Presentations from Maungawhau Information Technology](http://talk.maungawhau.net.nz)

[![Build Status](https://gitlab.com/jtmitchell/talk-maungawhau-net-nz/badges/master/pipeline.svg)](https://gitlab.com/jtmitchell/talk-maungawhau-net-nz/)

## Overview

This is the source for presentations I have given.

The site is generated from Markdown source and Web Starter Kit templates. 
Some of the presentations are created using reveal.js


## License

* The content and images for the site are copyright 2014 Maungawhau Information Technology Ltd.
* The code for the site is licensed under the the Apache 2.0 license
* Web Starter Kit [licence information](https://github.com/google/web-starter-kit/blob/master/LICENSE).
Web Starter Kit is copyright 2014 Google Inc

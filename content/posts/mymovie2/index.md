+++
title = "My Movie Talk 2"
slug = "mymovie2"
description = "Writing a Django API webapp"
date = "2015-06-15 18:30:00"
[taxonomies]
tags = ["django","python","travis-ci","github"]
[extra]
image = "posts/mymovie2/api-flow.png"
image_caption = "API requests"
+++

## &ldquo;Show me the money!&rdquo;

The second talk covers writing the server-side of the application.
Setting up a database to store user and movie data, then documenting and writing an API
to access the data.

<!--more-->

[![Talk 2](api-flow.png)](talk)

* [view the slides online](talk)
* [download the PDF version](MyMovie2.pdf)

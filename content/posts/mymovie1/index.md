+++
title = "My Movie Talk 1"
slug = "mymovie1"
description = "Starting a Django API webapp"
date = "2015-05-20 18:30:00"
[taxonomies]
tags = ["django","python","github","travis-ci"]
[extra]
image = "posts//mymovie1/mymovie-title.png"
image_caption = "Starting a project"
+++

## &ldquo;Louie, I think this is the beginning of a beautiful friendship.&rdquo;

The first talk covers the things that should be done before writing any code,
such as gathering requirements, and organising tools and services we will use later.

<!--more-->

[![Talk 1](mymovie-title.png)](talk)

 * [view the slides online](talk)
 * [download the PDF version](MyMovie1.pdf)

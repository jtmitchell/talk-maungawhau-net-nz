+++
title = "AWS Lambda and Python"
slug = "lightning-python-lambda"
description = "How to execute Python scripts on AWS Lambda"
date = "2015-08-18 11:30:00"
[extra]
image = "posts/lightning-python-lambda/title.png"
image_caption = "Python and Lambda"
[taxonomies]
tags = ["python","aws","lambda"]
+++

## Using AWS Lambda to run Python scripts

Slides for a lightning talk about running Python scripts from AWS Lambda.

<!--more-->

Lambda is Amazon's new service that allows execution of Java or Node.js functions
without having to deploy a full EC2 instance to run on.

Lambda is a compelling option for deploying endpoints for REST-based micro-services.

This lightning talk demonstrates how to use a Node.js wrapper to execute a
Python script.

Video of this talk from the [Auckland Python Users Group](http://www.meetup.com/NZPUG-Auckland/) is [available on YouTube](https://youtu.be/aQjLaG5WdCg).

[![Python and Lambda](title.png)](talk)

 * [view the slides online](talk)
 * [download the PDF version](LambdaPython.pdf)
 * [IPython Notebook with sample code calling the API](sample.ipynb)

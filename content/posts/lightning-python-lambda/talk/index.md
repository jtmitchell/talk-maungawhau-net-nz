+++
title = "AWS Lambda and Python"
description = "How to execute Python scripts on AWS Lambda"
author = "James Mitchell"
template = "slides.html"
[extra]
slides = "slides.html"
+++

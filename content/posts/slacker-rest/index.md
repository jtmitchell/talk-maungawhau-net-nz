+++
title = "Slacker's Guide to REST API"
slug = "slacker-rest"
description = "An simple introduction to REST API"
date = "2014-06-04 21:00:00"
[taxonomies]
tags = ["api","rest","django","python"]
[extra]
image = "posts//slacker-rest/title.png"
image_caption = ""
+++

An introduction to adding a REST API to a Python Django application, tools to access the API "by hand", and to document it.
<!--more-->

## [Slacker's Guide to REST API](Slackers-Guide-REST.pdf)

[![click to view the slides](title.png)](Slackers-Guide-REST.pdf)

Postman files from the presentation
* [Environment definition](Reeder-Local.postman_environment.txt)
* [Collection of endpoints](Reeder-Demo.postman_collection.txt)
